@extends('front-end.master')
@section('body')
		<div class="banner1">
			<div class="container">
				<h3><a href="">Home</a> / <span>Add To Cart</span></h3>
			</div>
		</div>
		<div class="clearfix"></div>

		<div class="content" style="margin-top: 40px; margin-bottom: 40px;">
			<div class="single-w13">
				<div class="container">
					<div class="row">
						<div class="col-md-11 col-md-offset-1">
							<h3 class="text-center text-success" style="margin-bottom: 40px;">My Shoping Cart</h3>
							<table class="table table-bordered">
								<tr class="bg-primary text-center">
									<th>Sl No</th>
									<th>Name</th>
									<th>Image</th>
									<th>Price Tk.</th>
									<th>quantity</th>
									<th>Total Price Tk.</th>
									<th>Action</th>
								</tr>
								@PHP($i=1)
								@PHP($sum=0)
								@foreach($cartProducts as $cartProduct)
								<tr>
									<td>{{ $i++}}</td>
									<td>{{ $cartProduct->name}}</td>
									<td><img src="{{ asset($cartProduct->options->image) }}" alt="" height="50" width="50"></td>
									<td>{{ $cartProduct->price}}</td>
									<td>
										{{ Form::open(['route'=>'update-cart' , 'method'=>'post'] ) }}
											
											<input type="number" name="qty"   value="{{ $cartProduct->qty }}"  min="1"/>
											<input type="hidden" name="rowId" value="{{ $cartProduct->rowId }}" min="1"/>
											<input type="submit" name="btn" class="btn btn-success"  value="update"/>

										{{ Form::close() }}
									</td>
									<td>{{ $total = $cartProduct->price*$cartProduct->qty}}</td>
									<td>
										<a class="btn btn-danger" href="{{ route('delete-cart-item', ['rowId'=>$cartProduct->rowId] ) }}" title="Delete">
											<span class="glyphicon glyphicon-trash"></span>
										</a>
									</td>
								</tr>
								<?php echo $sum =$sum+$total;?>
								@endforeach
							</table>
							<hr/>
							<table class="table table-bordered">
								<tr>
									<th>Item Total Price(Tk.)</th>
									<td>{{ $sum}}</td>
								</tr>
								<tr>
									<th>Vat Total Price(Tk.)</th>
									<td>{{ $vat = 0 }}</td>
								</tr>
								<tr>
									<th>Item Total Price(Tk.)</th>
									<td>{{ $orderTotal = $sum + $vat }}</td>
									<?php
										Session::put('orderTotal' ,$orderTotal);
									?>
								</tr>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-11 col-md-offset-1">
							<a href="{{ route('/')}}" class="btn btn-success pull-left">Continue Shopping</a>

							@if(Session::get('customerID') && Session::get('shepingId'))
							<a href="{{ route('checkout-payment')}}" class="btn btn-success pull-right">Check Out</a>
							@elseif(Session::get('customerID'))
							<a href="{{ route('checkout-sheping')}}" class="btn btn-success pull-right">Check Out</a>
							@else
							<a href="{{ route('checkout')}}" class="btn btn-success pull-right">Check Out</a>
							@endif
						</div>
					</div>
				</div>
			</div>	
		</div>
	<!--banner-->
@endsection