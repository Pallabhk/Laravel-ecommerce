@extends('admin.master')

@section('body')

<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="text-center text-success">
					Add Brand Form
				</h4>
			</div>	
				<div class="panel-body">
					 <h3 class="text-center text-success">{{ Session::get('message') }}</h3>
					 
					 {{ Form::open(['route'=>'new-brand', 'method'=>'POST', 'class'=>'form-horizontal']) }}
					<!-- 

					<div class="form-group ">
						 	{{ Form::label('brand_name', 'Brand Name:', ['class' => 'col-md-3  text-center'])}}
						<div class="col-md-9">
						 	{{ Form::text('brand_name','',['class'  =>'form-control']) }}
						 </div>
					 </div>
					-->
					<div class="from-group">
						<label for="brand_name" class="control-label col-md-4">Brand Name</label>
						<div class="col-md-8">
							<input type="text" name="brand_name" class="form-control"/></br>
							<span class="text-danger">{{ $errors->has('brand_name') ? $errors->first('brand_name') : ' ' }}</span>
						</div>
					</div>
					
					<div class="from-group">
						<label for="brand_dis"  class="control-label col-md-4">Brand Description</label>
						<div class="col-md-8">
							<textarea name="brand_dis" class="form-control"/></textarea></br>
							<span class="text-danger">{{ $errors->has('brand_dis') ? $errors->first('brand_dis') : ' ' }}</span>
						</div>
					</div>

					<div class="from-group">
						<label class="control-label col-md-4">Publication Status</label>
						<div class="col-md-8 radio">
							<label><input type="radio" name="publication_stutus" value="1"/>Published</label>
							<label><input type="radio" name="publication_stutus" value="0"/>Unpublished</label></br>
							<span class="text-danger">{{ $errors->has('publication_stutus') ? $errors->first('publication_stutus') : ' ' }}</span> 
						</div>
					</div>
					<div class="form-group">
							
							<div class="col-md-8 col-md-offset-4">
								<input type="submit" name="btn" class="btn btn-success btn-block" value="Save Brand" />
							</div>	
						</div>
					 {{ Form::close() }}
				
					
				</div>
			
		</div>
	</div>
</div>

@endsection