@extends('admin.master')

@section('body')

<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="text-center text-success">
					Edit Product
				</h4>
			</div>	
				<div class="panel-body">
					 <h3 class="text-center text-success">{{ Session::get('message') }}</h3>
					 
					 {{ Form::open(['route'=>'update-product', 'method'=>'POST', 'class'=>'form-horizontal' , 'enctype'=>'multipart/form-data', 'name' =>'editProductForm']) }}
					
					<div class="from-group">
						<label for="brand_name" class="control-label col-md-4">Category Name</label>
						<div class="col-md-8">
							<select class="form-control" name="category_id">
								<option>---Select Category Name---</option>
								@foreach( $category as $category)
								<option value="{{ $category->id }}">{{ $category->category_name }}</option>
						
								@endforeach
							</select></br>
							<span class="text-danger">{{ $errors->has('category_name') ? $errors->first('category_name') : ' ' }}</span>
						</div>
					</div>
					<div class="from-group">
						<label for="brand_name" class="control-label col-md-4">Brand Name</label>
						<div class="col-md-8">
							<select class="form-control" name="brand_id">
								<option>---Select Brand Name---</option>
								@foreach( $brand as $brand)
								<option value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
						
								@endforeach
							</select></br>
							<span class="text-danger">{{ $errors->has('brand_name') ? $errors->first('brand_name') : ' ' }}</span>
						</div>
					</div>
					<div class="from-group">
						<label for="product_name"  class="control-label col-md-4">Product Name</label>
						<div class="col-md-8">
							<input type="text" name="product_name" value="{{$product->product_name}}" class="form-control"/></br>
							<input type="hidden" name="product_id" value="{{ $product->id }}" class="form-control"/>
							<span class="text-danger">{{ $errors->has('product_name') ? $errors->first('product_name') : ' ' }}</span>
						</div>
					</div>
					<div class="from-group">
						<label for="product_price"  class="control-label col-md-4">Product Price</label>
						<div class="col-md-8">
							<input type="number" name="product_price" value="{{$product->product_price}}" class="form-control"/></br>
							<span class="text-danger">{{ $errors->has('product_price') ? $errors->first('product_price') : ' ' }}</span>
						</div>
					</div>
					<div class="from-group">
						<label for="product_quantity"  class="control-label col-md-4">Product Quantity</label>
						<div class="col-md-8">
							<input type="number" name="product_quantity" value="{{$product->product_quantity}}" class="form-control"/></br>
							<span class="text-danger">{{ $errors->has('product_quantity') ? $errors->first('product_quantity') : ' ' }}</span>
						</div>
					</div>
					<div class="from-group">
						<label for="short_dis"  class="control-label col-md-4">Short Description</label>
						<div class="col-md-8">
							<textarea name="short_dis" class="form-control"/>{{ $product->short_dis}}</textarea></br>
							<span class="text-danger">{{ $errors->has('short_dis') ? $errors->first('short_dis') : ' ' }}</span>
						</div>
					</div>
					<div class="from-group">
						<label for="long_dis"  class="control-label col-md-4">Long Description</label>
						<div class="col-md-8">
							<textarea name="long_dis" id="editor" class="form-control"/>{{ $product->long_dis}}</textarea></br>
							<span class="text-danger">{{ $errors->has('long_dis') ? $errors->first('long_dis') : ' ' }}</span>
						</div>
					</div>
					<div class="from-group">
						<label for="product_image"  class="control-label col-md-4">Product Image</label>
						<div class="col-md-8">
							<input type="file" name="product_image" accept="image/*" /></br>
							<img src="{{ asset($product->product_image) }}" alt="" height="150" width="100">
							<span class="text-danger">{{ $errors->has('product_image') ? $errors->first('product_image') : ' ' }}</span>
						</div>
					</div>
					<div class="from-group">
						<label class="control-label col-md-4">Publication Status</label>
						<div class="col-md-8 radio">
							<label><input type="radio" name="publication_stutus" {{ $product->publication_stutus == 1 ? 'checked' : '' }} value="1"/>Published</label>
							<label><input type="radio" name="publication_stutus" {{ $product->publication_stutus==0 ? 'checked' : '' }} value="0"/>Unpublished</label></br>
							<span class="text-danger">{{ $errors->has('publication_stutus') ? $errors->first('publication_stutus') : ' ' }}</span>
						</div>
					</div>
					<div class="form-group">
							
							<div class="col-md-8 col-md-offset-4">
								<input type="submit" name="btn" class="btn btn-success btn-block" value="Update Product Info" />
							</div>	
						</div>
					 {{ Form::close() }}
				
					
				</div>
			
		</div>
	</div>
</div>
<script>
	document.forms['editProductForm'].elements['category_id'].value={{ $product->category_id }}
	document.forms['editProductForm'].elements['brand_id'].value={{ $product->brand_id }}
</script>
@endsection