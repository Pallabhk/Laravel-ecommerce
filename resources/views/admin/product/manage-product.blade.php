@extends('admin.master')

@section('body')
	<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="text-center text-success">
					 All Brands
				</h4>
			</div>	
				<div class="panel-body">
					<h3 class="text-center text-success">{{ Session::get('message')}}</h3>
					<div class="table-responsive">
						<table class="table table-bordered">
						<tr class="bg-primary">
							<th>Sl No</th>
							<th>Category Name</th>
							<th>Brand Name</th>
							<th>Product Name</th>
							<th>Product Price</th>
							<th>Product Quantity</th>
							<th>Short Description</th>
							<th>Long Description</th>
							<th>Product Image</th>
							<th>Publication Status</th>
							<th>Action</th>
						</tr>
						
						@php($i=1)
						@foreach($products as $product)
						<tr>

							<td class="col-md-1">{{ $i++ }}</td>
							<td class="col-md-1">{{ $product->category_name }}</td>
							<td class="col-md-1">{{ $product->brand_name }}</td>
							<td class="col-md-1">{{ $product->product_name }}</td>
							<td class="col-md-1">{{ $product->product_price }}</td>
							<td class="col-md-1">{{ $product->product_quantity }}</td>
							<td class="col-md-1">{{ $product->short_dis }}</td>
							<td class="col-md-1">{{ $product->long_dis }}</td>
							<td class="col-md-1"><img src="{{ asset($product->product_image) }}" alt="" height="100" width="100"></td>
							<td class="col-md-1">{{ $product->publication_stutus == 1 ? 'Published' : 'Unpublished' }}</td>
							<td class="col-md-2">

								@if( $product->publication_stutus ==1)
								<a href="{{ route('unpublished-product',['id'=>$product->id])}}" class="btn btn-info btn-xs">
									<span class="glyphicon glyphicon-arrow-up"></span>
								</a>
								@else
								<a href="{{ route('published-product',['id'=>$product->id])}}" class="btn btn-warning btn-xs">
									<span class="glyphicon glyphicon-arrow-down"></span>
								</a>
								@endif
								<a href="{{ route('edit-product',['id'=>$product->id])}}" class="btn btn-edit btn-xs">
									<span class="glyphicon glyphicon-edit"></span>
								</a>
								<a href="{{ route('delete-product',['id'=>$product->id])}}" class="btn btn-danger btn-xs">
									<span class="glyphicon glyphicon-trash"></span>
								</a>
							</td>
						</tr>
						@endforeach
						</table> 
					</div>
				</div>
		</div>
	</div>
</div>
@endsection