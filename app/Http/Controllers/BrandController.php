<?php
 namespace App\Http\Controllers;
 use App\Brand;
 use Illuminate\Http\Request;

class BrandController extends Controller
{
	
	public function index(){

		return view('admin.brand.add-brand');
	}  

	 
	public function saveInfo(Request $request){

		$this->validate($request ,[
			'brand_name'          => 'required|regex:/^[\pL\s\-]+$/u| max:15|min:4', 
			'brand_dis'           => 'required',
			'publication_stutus'  => 'required'
		]);

 
		$brand = new Brand();
		$brand ->brand_name =$request->brand_name;
		$brand ->brand_dis  =$request->brand_dis;
		$brand ->publication_stutus=$request->publication_stutus;
		$brand->save();
			
		return redirect('/brand/add')->with('message' ,'Brand Info save successfully');
	}

	public function manageBrandInfo(){
		$brands = Brand::all();

		//return $brands;
		return view('admin.brand.manage-brand' ,['brands'=>$brands]);
	} 
	public function unpublishedBrandInfo($id){
		$brand = Brand::find($id);
		$brand ->publication_stutus=0;
		$brand->save();

		return redirect('/brand/manage')->with('message' , 'Brand Info unpublished');
	}
	public function publishedBrandInfo($id){
		$brand = Brand::find($id);
		$brand ->publication_stutus=1;
		$brand->save();

		return redirect('/brand/manage')->with('message' , 'Brand Info published');
	}
	public function editBrandinfo($id){

		$brand = Brand::find($id);
		// return $brand;
		return view('admin.brand.edit-brand',['brand' =>$brand]);
	}

	public function updateBrandInfo(Request $request){

		//return $request->all();
		$this->validate($request ,[
			'brand_name'          => 'required|regex:/^[\pL\s\-]+$/u| max:15|min:4',
			'brand_dis'           => 'required',
			'publication_stutus'  => 'required'
		]);

		$brand = Brand::find($request->brand_id);

		$brand ->brand_name         = $request->brand_name;
 		$brand ->brand_dis          = $request->brand_dis;
 		$brand ->publication_stutus = $request->publication_stutus;
 		$brand ->save();

 		return redirect('/brand/manage')->with('message', 'Brand Info Update Successfully');
	}

	public function deleteBrandInfo($id){

		$brand= Brand::find($id);
		$brand->delete();

 		return redirect('/brand/manage')->with('message', 'Brand Info Delete Successfully');
	}
}
