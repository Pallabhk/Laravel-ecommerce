<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Sheping;
use App\Order;
use App\Payment;
use App\OrderDetail;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

use Mail;
use Session;
class CheckOutController extends Controller
{
   public function index(){

   	return view('front-end.checkout.checkout-here');
   }

   public function customerSignUp(Request $request) {

   		$this->validate($request,[
   			'email' => 'unique:customers, email'
   		]);
   		$customer = new Customer();
   		$customer->first_name =$request->first_name;
   		$customer->last_name  =$request->last_name;
   		$customer->email      =$request->email;
   		$customer->password   =bcrypt($request->password);
   		$customer->phone      =$request->phone;
   		$customer->address    =$request->address;
   		$customer->save();

   		

   		$customerID =$customer->id;
   		Session::put('customerID' ,$customerID);
   		Session::put('customerName', $customer->first_name.' '.$customer->last_name);

   		$data =$customer->toArray();
			Mail::send('front-end.mails.confirmation-mail', $data, function($message) use ($data){
   			$message->to($data['email']);
   			$message->subject('Confirmation mail');
   		});

			return redirect('checkout/sheping');

   }


   public function customerLogin( Request $request){
   		$customer = Customer::where('email' ,$request->email)->first();

   		//return $customer;
   		if (password_verify($request->password, $customer->password)) {
   			
   			echo 'Password is valid';
   		}else{
   			echo "Password is invalid";
   		}

   }

   public function  sheping(){

   		$customer = Customer::find(Session::get('customerID'));
   	return view('front-end.checkout.sheping',['customer'=>$customer]);
   }

   public function shepingSaveInfo(Request $request){

   		$sheping = new Sheping();
   		$sheping->full_name  =$request->full_name;
   		$sheping->email      =$request->email;
   		$sheping->phone      =$request->phone;
   		$sheping->address    =$request->address;
   		$sheping->save();

   		Session::put('shepingId', $sheping->id);

   		return redirect('/checkout/payment');
   }

   public function paymentForm(){

   	return view('front-end.checkout.payment');
   }

   public function orderSaveInfo(Request $request){

   		//return $request->all();
   		$paymentType = $request->payment_type;
   		if ($paymentType == 'cash') {
   			$order = new Order();
   			$order->customer_id = Session::get('customerID');
   			$order->sheping_id  = Session::get('shepingId');
   			$order->order_total = Session::get('orderTotal');
   			$order->save();

   			$payment = new Payment();
   			$payment->order_id =$order->id;
   			$payment->payment_type=$paymentType;
   			$payment->save();


   			$cartProducts = Cart::content();
   			foreach ($cartProducts as $cartProduct) {
   				$orderDetail =new OrderDetail();
   				$orderDetail->order_id =$order->id;
   				$orderDetail->product_id=$cartProduct->id;
   				$orderDetail->product_name=$cartProduct->name;
   				$orderDetail->product_price=$cartProduct->price;
   				$orderDetail->product_quantity=$cartProduct->qty;
   				$orderDetail->save();

   			}
   			Cart::destroy();
   			return redirect('/complete/order');
   		}else if ($paymentType  == 'paypal') {
   			
   		}else if ($paymentType  == 'bkash') {

   		}
   }

   public function orderComplete(){
   	return "Success";
   }
}
