<?php

namespace App\Http\Controllers;

use App\Category;
use App\Brand;
use App\Product;
use Image;
use DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{
   public function index(){

   		$categories = Category::where('publication_status', 1)->get();
   		$brands    = Brand::where('publication_stutus' , 1)->get();
   	
   	return view('admin.product.add-product',[

   			'categories' => $categories,
   			'brands'     => $brands,
   	]);
   }

   protected function productInfoValidate($request){

   		$this->validate($request, [
   			'product_name'       => 'required|regex:/^[\pL\s\-]+$/u|max:15|min:4',
   			'product_price'      => 'required',
   			'product_quantity'   => 'required',
   			'short_dis'          => 'required',
   			'long_dis'           => 'required',
   			'product_image'      => 'required',
   			'publication_stutus' => 'required',
   			'product_price'      => 'required',
   			'product_price'      => 'required',
   		]);
	
   }
   protected function imageUpload($request){

   		//$imageName    = $productImage->getClientOriginalName();
   		//$imageName    =$productImage->getClientOriginalExtension();

    	   $productImage = $request->file('product_image');
   		$fileType     =$productImage->getClientOriginalExtension();
   		$imageName    =$request->product_name.'.'.$fileType;
   		$directory    ='product-images/';
   		$imageUrl     = $directory.$imageName;
   		//$productImage->move($directory, $imageName);
   		Image::make($productImage)->resize(200,200)->save($imageUrl);

   		return $imageUrl;
   }
   protected function saveProductBasicInfo($request ,$imageUrl){

   	   $product   = new Product();
   		$product ->category_id     =$request->category_id;
   		$product ->brand_id        =$request->brand_id;
   		$product ->product_name    =$request->product_name;
   		$product ->product_price   =$request->product_price;
   		$product ->product_quantity=$request->product_quantity;
   		$product ->short_dis       =$request->short_dis;
   		$product ->long_dis        =$request->long_dis;
   		$product ->product_image   =$imageUrl;
   		$product ->publication_stutus=$request->publication_stutus;
   		$product->save();
   } 
   public function saveProductInfo(Request $request){

   		$productImage= $request->file('product_image');
   		$imageName =$productImage->getClientOriginalExtension();
   		//return $imageName;


   		$this->productInfoValidate($request);
   		$imageUrl=$this->imageUpload($request);
   		$this->saveProductBasicInfo($request ,$imageUrl);
   		
   		

   		return redirect('/product/add')->with('message','Product info save successfully');
   		
   		
   }

   public function manageProductInfo(){
   		$products = DB::table('products')
   						->join('categories','products.category_id', '=', 'categories.id')
   						->join('brands','products.brand_id', '=', 'brands.id' )
   						->select('products.*','categories.category_name','brands.brand_name')
   						->get();
   		//return $products;
   	return view('admin.product.manage-product', ['products'=>$products]);
   }
   

	public function unpublishedProductInfo($id){
		$product = Product::find($id);
		$product->publication_stutus=0;
		$product->save();

		return redirect('/product/manage')->with('message','Product Info unpublished');
	}

	public function  publishedProductInfo($id){
		$product =Product::find($id);
		$product->publication_stutus=1;
		$product->save();

		return redirect('/product/manage')->with('message', 'Product Info unpublished');
	}
	public function editProductInfo($id){
		
		   $category = Category::where('publication_status', 1)->get();
   		$brand    = Brand::where('publication_stutus' , 1)->get();
   		$product  =Product::find($id);
		//return $brand;
		return view('admin.product.edit-product',[
         'product'=>$product ,
         'brand' =>$brand, 
         'category'=>$category
      ]);
	}


   /********Update work**********/

   public function updateProductInfoBasic($product,$request ,$imageUrl=null){

         $product ->category_id     =$request->category_id;
         $product ->brand_id        =$request->brand_id;
         $product ->product_name    =$request->product_name;
         $product ->product_price   =$request->product_price;
         $product ->product_quantity=$request->product_quantity;
         $product ->short_dis       =$request->short_dis;
         $product ->long_dis        =$request->long_dis;
         if ($imageUrl) {
            $product->product_image =$imageUrl;
         }
         $product ->publication_stutus=$request->publication_stutus;
         $product->save();

     
   }
	public function updateProductInfo(Request $request ){
     $productImage = $request->file('product_image');
     $product =Product::find($request->product_id);

     if ($productImage) {
       unlink($product->product_image);
       $imageUrl=$this->imageUpload($request);
       $this->updateProductInfoBasic($product, $request, $imageUrl);
     }else{
      $this->updateProductInfoBasic($product, $request);
     }

      return redirect('/product/manage')->with('message', 'Product Info Update Successfully');	
	}
	
	 

    public function deleteProductInfo($id){
      $product= Product::find($id);
      $product->delete();

      return redirect('/product/manage')->with('message', 'Product Info Delete Successfully');
    }

}
