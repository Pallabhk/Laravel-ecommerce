<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('welcome');
});

Route::get(' / ',function(){

	//return 'Hello laravel';
	return view('home');
});




//Route::get('', 'StudentController@index');

Route::get('/' , [

			'uses' =>'StudentController@index',
			'as'   =>'/'
]);	

Route::get('/about' , [
		'uses'    =>'StudentController@about',
		'as'      =>'/about'
]);
Route::get(' /pallab',function(){

	return 'Hello Pallab';
});


*/

/* Ecommerce Project start */


Route::get('', [

	'uses'  => 'NewShopController@index',
	'as'    =>'/'
 ]);

Route::get('/category-product/{id}', [

	'uses'  =>'NewShopController@category',
	'as'    =>'category-product'

]);

Route::get('/product-details/{id}/{name}', [

	'uses'  =>'NewShopController@productdetails',
	'as'    =>'product-details'

]);

Route::post('/cart/add', [

	'uses'  =>'CartController@addToCart',
	'as'    =>'add-to-cart'

]);

Route::get('/cart/show', [

	'uses'  =>'CartController@showCart',
	'as'    =>'show-cart'

]);

Route::get('/cart/delete/{id}', [

	'uses'  =>'CartController@deleteCart',
	'as'    =>'delete-cart-item'

]);
Route::post('/cart/update', [

	'uses'  =>'CartController@updateCart',
	'as'    =>'update-cart'

]);
//CheckOut 

Route::get('/checkout',[
	'uses' =>'CheckOutController@index',
	'as'   =>'checkout'
]);
Route::get('/checkout/sheping',[
	'uses' =>'CheckOutController@sheping',
	'as'   =>'checkout-sheping'
]);

Route::get('/checkout/payment',[
	'uses' =>'CheckOutController@paymentForm',
	'as'   =>'checkout-payment'
]);

Route::get('/complete/order',[
	'uses' =>'CheckOutController@orderComplete',
	'as'   =>'complete-order'
]);

Route::post('/checkout/order',[
	'uses' =>'CheckOutController@orderSaveInfo',
	'as'   =>'new-order'
]);


Route::post('/sheping/save',[
	'uses' =>'CheckOutController@shepingSaveInfo',
	'as'   =>'new-sheping'
]);

Route::post('/customer/registration',[
	'uses' =>'CheckOutController@customerSignUp',
	'as'   =>'customer-sign-up'
]);

Route::post('/customer/customer-login',[
	'uses' =>'CheckOutController@customerLogin',
	'as'   =>'customer-login'
]);

Route::get('/mail-us',[

	'uses' =>'NewShopController@mail',
	'as'   =>'mail-us'
]);

Route::get('/login-us',[

	'uses' =>'NewShopController@login',
	'as'   =>'login-us'
]);

Route::get('/checkout-product',[

	'uses' =>'NewShopController@checkout',
	'as'   =>'checkout-product'
]);
Route::get('/register-account',[

	'uses' =>'NewShopController@register',
	'as'   =>'register-account'
]);

/********* Category Info Start**********/

Route::get('/category/add', [

		'uses'  => 'CategoryController@index',
		'as'    => 'add-category'
]);

Route::get('/category/manage', [

		'uses'  => 'CategoryController@manageCategoryInfo',
		'as'    => 'manage-category'
]);

Route::post('/category/new-category', [

		'uses'  => 'CategoryController@saveCategory',
		'as'    => 'new-category'
]);

Route::get('/category/unpublished/{id}', [

		'uses'  => 'CategoryController@unpublishedCategoryinfo',
		'as'    => 'unpublished-category'
]);
Route::get('/category/published/{id}', [

		'uses'  => 'CategoryController@publishedCategoryinfo',
		'as'    => 'published-category'
]);
//Edit category
Route::get('/category/edit/{id}', [

		'uses'  => 'CategoryController@editCategoryinfo',
		'as'    => 'edit-category'
]);

//Update Category
Route::post('/category/update', [

		'uses'  => 'CategoryController@updateCategoryinfo',
		'as'    => 'update-category'
]);

//Delete Category
Route::get('/category/delete/{id}', [

		'uses'  => 'CategoryController@deleteCategoryinfo',
		'as'    => 'delete-category'
]);

/********* Category Info End**********/

/********* Brand Info Start**********/
Route::get('/brand/add', [

	'uses'  => 'BrandController@index', 
	'as'    => 'add-brand'
]);

Route::post('/brand/new-brand',[

	'uses'  => 'BrandController@saveInfo',
	'as'    => 'new-brand'
]);

Route::get('/brand/manage',[
	'uses' =>'BrandController@manageBrandInfo',
	'as'   =>'manage-brand'

]);

Route::get('/brand/unpublished/{id}',[
	'uses' =>'BrandController@unpublishedBrandInfo',
	'as'   =>'unpublished-brand'

]);

Route::get('/brand/published/{id}',[

	'uses' =>'BrandController@publishedBrandInfo',
	'as'   =>'published-brand'
]);
/* Edit Brand */
Route::get('/brand/edit/{id}', [

		'uses'  => 'BrandController@editBrandinfo',
		'as'    => 'edit-brand'
]);
/* Edit Brand End*/
/* Update Brand */

Route::post('/brand/update',[
	'uses' => 'BrandController@updateBrandInfo',
	'as'   => 'update-brand'
]);

/*Delete ID*/

Route::get('/brand/delete/{id}',[

	'uses'  => 'BrandController@deleteBrandInfo',
	'as'    => 'delete-brand'
]);

/********* Brand Info End**********/

/********* Product Info Start**********/


Route::get('/product/add',[

	'uses' =>'ProductController@index',
	'as'   =>'add-product'
]);

Route::post('/product/save',[

	'uses'  => 'ProductController@saveProductInfo',
	'as'    => 'new-product'
]);

Route::get('/product/manage',[

	'uses'  => 'ProductController@manageProductInfo',
	'as'    => 'manage-product'
]);


Route::get('/product/unpublished/{id}',[

		'uses'=>'ProductController@unpublishedProductInfo',
		'as'  =>'unpublished-product'

]);
Route::get('/product/published/{id}',[
		'uses'  => 'ProductController@publishedProductInfo',
		'as'    =>'published-product'
]);

Route::get('/product/edit/{id}',[
	'uses'=>'ProductController@editProductInfo',
	'as'  =>'edit-product'
]);

/*Update Product*/

Route::post('/product/update',[
	'uses' =>'ProductController@updateProductInfo', 
	'as'  =>'update-product'
]);

Route::get('/product/delete/{id}',[

	'uses'  => 'ProductController@deleteProductInfo',
	'as'    => 'delete-product'
]);
/********* Product Info End**********/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
